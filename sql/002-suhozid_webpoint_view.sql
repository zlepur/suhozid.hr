-- create suhozid_web view

BEGIN;

CREATE VIEW suhozid_webpoint_view AS
    SELECT id, anonymous_author, image_name, description, st_astext(point) as coord, point, image from suhozid_webpoint;

GRANT SELECT, UPDATE, DELETE ON suhozid_webpoint_view TO normal_user;

CREATE RULE update_suhozid_webpoint_view AS ON UPDATE TO suhozid_webpoint_view
-- WHERE
DO INSTEAD
UPDATE suhozid_webpoint SET anonymous_author = NEW.anonymous_author, point = NEW.point, image_name = NEW.image_name, description = NEW.description
WHERE id=OLD.id;

CREATE RULE delete_suhozid_webpoint_view AS ON DELETE TO suhozid_webpoint_view
DO INSTEAD
DELETE FROM suhozid_webpoint WHERE id=OLD.id;

COMMIT;
