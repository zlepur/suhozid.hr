/* Extend jQuery */
$.fn.serializeObject = function() {
    var o = {};
    var a = this.serializeArray();
    $.each(a, function() {
        if (o[this.name]) {
            if (!o[this.name].push) {
                o[this.name] = [o[this.name]];
            }
            o[this.name].push(this.value || '');
        } else {
            o[this.name] = this.value || '';
        }
    });
    return o;
};


!function ($) {

  var CEModal = function (element, options) {
    this.options = options;
    this.element = $(element);
    this.init();
  };


  CEModal.prototype = {
    constructor: CEModal,

    init: function() {
        var self=this;

        this.dialog_template = _.template([
            '<div class="modal" id="myModal">',
                '<div class="modal-header">',
                '<button class="close" data-dismiss="modal">×</button>',
                '<h3 class="title"></h3>',
                '</div>',
            '<div class="modal-body">',
            '</div>',
            '<div class="modal-footer">',
            '</div>',
            '</div>'
            ].join(''));

        this.element.html(this.dialog_template({}));

        this.mydialog = this.element.modal({
            show: false//,
            //modal: true
        });

        this.title = this.mydialog.find('.title');
        this.body = this.mydialog.find('.modal-body');
        this.buttons = this.mydialog.find('.modal-footer');

        //destroy on close
        this.mydialog.on("hidden", function(event, ui) {
            self._destroy();
        });

        self.title.html('Dodatne informacije');

        this.url = this.options.url;

        //init edit state
        this._edit_state();

        this.element.bind('show_editform', function (evt, data) {
            self.body.html(self._update_form_fields(data));


            self.buttons.html('<button type="button" class="btn btn-primary save-btn" data-loading-text="Spremam...">Spremi</button>');
            self.save_button = self.buttons.find('.save-btn').button();

            self.save_button.on('click', function (evt) {
                self.element.trigger('validateform');
            });
            self.mydialog.modal('show');
        });

        this.element.bind('validateform', function (evt) {
            self.save_button.button('loading');
            self._validateform();
        });

        this.element.bind('delete_model', function (evt) {
            self.options.model.destroy({'wait':true});
            self.mydialog.modal('hide');
        });


        this.element.bind('saveform', function (evt, data){
            // on create
            // if (self.options.type === 'create') {
            // self.options.model.create(ed_form.serializeObject(), {
            // var ed_form = $($.parseHTML(data));
            self.save_button.button('reset');
            self.mydialog.modal('hide');

            $APP.trigger('point-saved');

        });

        this.element.bind('validate_failed', function (evt,data) {
            self.body.html(data);
            self.save_button.button('reset');
            //self.title.html(self.mod_title);
        });

        this.element.bind('edit_again', function (evt) {
            //restore state
        });
    },

    _update_form_fields: function(form_data) {
        var myForm = $.parseHTML(form_data);
        _.each(this.options.form_values, function(value, key) {
            $(myForm).find('#id_'+key).val(value);
        });
        return myForm;
    },

    _validateform: function () {
        var self=this;
        var forma = self.mydialog.find('form');
        forma.ajaxForm({
          data: forma.serialize(),
          dataType: 'html',
          type: 'POST',
          url: self.url,
            statusCode:{
                200: function(body) {
                    //if ok, trigger saveform
                    self.element.trigger('saveform', body);
                },
                261: function(body) {
                    self.element.trigger('validate_failed', body);
                }
            },
            error: function(xhr, type) {
                alert('Greška na serveru!');
                self.save_button.button('reset');
            }
        });

        forma.submit();
    },

    _edit_state: function () {
        var self=this;
        var edit_html = null;
        $.ajax({
            type: 'GET',
            url: self.url,
            //dataType: 'json',
            async: true,
            statusCode:{
                200: function(body) {
                    self.element.trigger('show_editform', body);
                        //send event, widget level
                    }
                },
                error: function(xhr, type) {
                    alert('Greška na serveru, u trenutku preuzimanja forme!');
                } // type is a string ('error' for HTTP errors, 'parsererror' for invalid JSON)
            });
    },

    _destroy: function () {
        //debugger;
        this.element.remove();
    }
  };

  $.fn.ce_modal = function ( option ) {
    return this.each(function () {
      var $this = $(this)
      , data = $this.data('CEModal')
        , options = typeof option == 'object' && option;
      if (!data) $this.data('CEModal', (data = new CEModal(this, options)));
      if (typeof option == 'string') data[option]();
    });
  };

}(window.jQuery); //CEModal
