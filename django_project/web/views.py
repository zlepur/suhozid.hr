from django.http import HttpResponseRedirect
from django.views.generic.base import TemplateView, View
from django.views.generic import UpdateView
from django.core.urlresolvers import reverse_lazy
from django.contrib.auth import logout as auth_logout

from django.contrib.auth.models import User

from suhozid.models import Locality

from .utils import CachePage
from .forms import UserForm


@CachePage(10 * 60)
class HomeView(TemplateView):
    template_name = 'index.html'

    def get_context_data(self, **kwargs):
        context = super(HomeView, self).get_context_data(**kwargs)

        context['last_20_localites'] = (
            locality.get_object_info()
            for locality in (
                Locality.objects.select_subclasses()
                .order_by('-created').all()[:20]
            )
        )
        return context


@CachePage(10 * 60)
class AboutProjectView(TemplateView):
    template_name = 'about.html'


@CachePage()
class ContactView(TemplateView):
    template_name = 'contact.html'


class LogoutUser(View):
    def get(self, request, *args, **kwargs):
        auth_logout(request)
        return HttpResponseRedirect('/')


class LoginPage(TemplateView):
    template_name = 'login.html'


class ProfilePage(UpdateView):
    template_name = 'user_profile.html'
    form_class = UserForm
    success_url = reverse_lazy('profile_page')

    def get_object(self):
        myUser = User.objects.get(pk=self.request.user.pk)
        return myUser
