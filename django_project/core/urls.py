from django.conf.urls import patterns, include, url

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()

urlpatterns = patterns(
    '',
    # Examples:
    # url(r'^$', 'suhozid_web.views.home', name='home'),
    # url(r'^suhozid_web/', include('suhozid_web.foo.urls')),

    # Uncomment the admin/doc line below to enable admin documentation:
    # url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    # url(r'^admin/', include(admin.site.urls)),

    url(r'^markitup/', include('markitup.urls')),
    url('', include('social.apps.django_app.urls', namespace='social')),

    url(r'', include('web.urls')),
    url(r'', include('suhozid.urls')),
)
