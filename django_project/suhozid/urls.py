from django.conf.urls import patterns, url

from .views import (
    CreateWebPoint, MyDryWallPortalView, GetFeatureInfo, LocalityDetailView,
    LocalityEditView, GetLocalityInfo, LocalityReviewView
)

urlpatterns = patterns(
    '',
    url(r'^createwebpoint/$', CreateWebPoint.as_view(), name='createwebpoint'),
    url(r'^moj_suhozid/$', MyDryWallPortalView.as_view(), name='my_drywall'),
    url(r'^getfeatureinfo/$', GetFeatureInfo.as_view(), name='getfeatureinfo'),
    url(r'^getlocalityinfo/$',
        GetLocalityInfo.as_view(), name='getlocalityinfo'),
    url(r'^detail/(?P<pk>\d+)/$',
        LocalityDetailView.as_view(), name='localitydetail'),
    url(r'^edit/(?P<pk>\d+)/$',
        LocalityEditView.as_view(), name='localityedit'),
    url(r'^review/(?P<pk>\d+)/$',
        LocalityReviewView.as_view(), name='localityreview'),
)
