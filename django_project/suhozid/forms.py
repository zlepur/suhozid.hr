import django.forms as forms
from crispy_forms.helper import FormHelper

from markitup.widgets import MarkItUpWidget

from .models import WebPoint, Locality


class WebPointForm(forms.ModelForm):
    short_text = forms.HiddenInput()
    main_text = forms.HiddenInput()

    class Meta:
        model = WebPoint

        fields = ['image_name', 'image', 'description', 'point']

        widgets = {
            'point': forms.HiddenInput()
        }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.help_text_inline = True
        self.helper.html5_required = False

        super(WebPointForm, self).__init__(*args, **kwargs)


class WebPointFormAnonymous(forms.ModelForm):
    class Meta:
        model = WebPoint

        fields = [
            'image_name', 'image', 'description', 'anonymous_author', 'point']

        widgets = {
            'point': forms.HiddenInput()
        }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.help_text_inline = True
        self.helper.html5_required = False

        super(WebPointFormAnonymous, self).__init__(*args, **kwargs)


class LocalityEditForm(forms.ModelForm):
    class Meta:
        model = Locality

        fields = [
            'locality_title', 'author', 'short_text', 'main_text'
        ]
        widgets = {
            'short_text': MarkItUpWidget,
            'main_text': MarkItUpWidget
        }

    def __init__(self, *args, **kwargs):
        self.helper = FormHelper()
        self.helper.help_text_inline = True
        self.helper.html5_required = False
        self.helper.form_tag = False

        super(LocalityEditForm, self).__init__(*args, **kwargs)
