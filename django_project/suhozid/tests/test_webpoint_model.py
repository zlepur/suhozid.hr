from django.test import TestCase

from model_factories import WebPointF


class TestWebpointModel(TestCase):
    def test_get_short_text_short_text_exists(self):
        """
        get_short_text() should return short_text attribute
        enclosed in <p> tag if it exists.
        """
        TestWebpoint = WebPointF(**{'short_text': 'TEST'})
        self.assertEqual(TestWebpoint.get_short_text(), '<p>TEST</p>')

    def test_get_short_text_short_text_doesnt_exists(self):
        """
        get_short_text() should return None if short_text attribute
        does not exist.
        """
        TestWebpoint = WebPointF()
        self.assertEqual(TestWebpoint.get_short_text(), None)

    def test_get_short_info_short_text_exists(self):
        """
        If short_text exists it has priority over other values.
        It should also be enclosed in <p> tag.
        """
        TestWebpoint = WebPointF(**{'short_text': 'TEST'})
        shortTextValue = TestWebpoint.get_short_info()['short_text']
        self.assertEqual(shortTextValue, '<p>TEST</p>')

    def test_get_short_info_desc_exists(self):
        """
        If short_text doesn't exists and description exists, return description
        """
        TestWebpoint = WebPointF(**{'description': 'TEST'})
        shortTextValue = TestWebpoint.get_short_info()['short_text']
        self.assertEqual(shortTextValue, 'TEST')

    def test_get_short_info_short_text_no_values(self):
        """
        In case of no values in short_text or description,
        return an empty string.
        """
        TestWebpoint = WebPointF()
        shortTextValue = TestWebpoint.get_short_info()['short_text']
        self.assertEqual(shortTextValue, '')

    def test_get_short_info_author_exists(self):
        """
        If author exists it has priority over other values.
        """
        TestWebpoint = WebPointF()
        authorValue = TestWebpoint.get_short_info()['author']
        self.assertEqual(authorValue, TestWebpoint.author.username)

    def test_get_short_info_anon_author_exists(self):
        """
        If no author exists, but anon author exists return it.
        """
        TestWebpoint = WebPointF(**{
            'author': None,
            'anonymous_author': 'anon'}
        )
        authorValue = TestWebpoint.get_short_info()['author']
        self.assertEqual(authorValue, 'anon')

    def test_get_short_info_no_author_exists(self):
        """
        If no author exists, return 'Nije poznat'
        """
        TestWebpoint = WebPointF(**{'author': None})
        authorValue = TestWebpoint.get_short_info()['author']
        self.assertEqual(authorValue, 'Nije poznat')

    def test_get_detailed_info_main_text_exists(self):
        """
        If main_text exists it has priority over other values.
        It should also be enclosed in <p> tag.
        """
        TestWebpoint = WebPointF(**{'main_text': 'TEST'})
        mainTextValue = TestWebpoint.get_detailed_info()['main_text']
        self.assertEqual(mainTextValue, '<p>TEST</p>')

    def test_get_detailed_info_desc_exists(self):
        """
        If description exists, but main_text doesn't, return description
        as main_text.
        """
        TestWebpoint = WebPointF(**{'description': 'TEST'})
        mainTextValue = TestWebpoint.get_detailed_info()['main_text']
        self.assertEqual(mainTextValue, 'TEST')

    def test_get_detailed_info_main_text_no_values(self):
        """
        In case of no values in main_text or description,
        return an empty string.
        """
        TestWebpoint = WebPointF()
        mainTextValue = TestWebpoint.get_detailed_info()['main_text']
        self.assertEqual(mainTextValue, '')

    def test_get_detailed_info_author_exists(self):
        """
        If author exists it has priority over other values.
        """
        TestWebpoint = WebPointF()
        authorValue = TestWebpoint.get_detailed_info()['author']
        self.assertEqual(authorValue, TestWebpoint.author.username)

    def test_get_detailed_info_anon_author_exists(self):
        """
        If no author exists, but anon author exists return it.
        """
        TestWebpoint = WebPointF(**{
            'author': None,
            'anonymous_author': 'anon'}
        )
        authorValue = TestWebpoint.get_detailed_info()['author']
        self.assertEqual(authorValue, 'anon')

    def test_get_detailed_info_no_author_exists(self):
        """
        If no apropriate author exists, return 'Nije poznat'
        """
        TestWebpoint = WebPointF(**{'author': None})
        authorValue = TestWebpoint.get_detailed_info()['author']
        self.assertEqual(authorValue, 'Nije poznat')

    def test_get_detailed_info_locality_title_exists(self):
        """
        If locality_title exists it has priority over other values.
        """
        TestWebpoint = WebPointF()
        titleValue = TestWebpoint.get_detailed_info()['title']
        self.assertEqual(titleValue, TestWebpoint.locality_title)

    def test_get_detailed_info_image_name_exists(self):
        """
        If no locality_title exists, but image_name exists return it.
        """
        TestWebpoint = WebPointF(**{'locality_title': ''})
        titleValue = TestWebpoint.get_detailed_info()['title']
        self.assertEqual(titleValue, TestWebpoint.image_name)

    def test_get_detailed_info_no_title_value(self):
        """
        If no apropriate title exists, return an empty string.
        """
        TestWebpoint = WebPointF(**{'locality_title': '', 'image_name': ''})
        titleValue = TestWebpoint.get_detailed_info()['title']
        self.assertEqual(titleValue, '')

    def test_get_original_data(self):
        """
        Simple test for ID.
        """
        TestWebpoint = WebPointF()
        IDValue = TestWebpoint.get_original_data()[0][1]
        self.assertEqual(IDValue, TestWebpoint.id)

    def test_get_object_info_locality_title_exists(self):
        """
        If locality_title exists it has priority over other values.
        """
        TestWebpoint = WebPointF()
        nameValue = TestWebpoint.get_object_info()['name']
        self.assertEqual(nameValue, TestWebpoint.locality_title)

    def test_get_object_info_image_name_exists(self):
        """
        If no locality_title exists, but image_name exists return it.
        """
        TestWebpoint = WebPointF(**{'locality_title': ''})
        nameValue = TestWebpoint.get_object_info()['name']
        self.assertEqual(nameValue, TestWebpoint.image_name)

    def test_get_object_info_no_name_exists(self):
        """
        If no apropriate name exists, return 'Bez naziva'.
        """
        TestWebpoint = WebPointF(**{'locality_title': '', 'image_name': ''})
        nameValue = TestWebpoint.get_object_info()['name']
        self.assertEqual(nameValue, 'Bez naziva')
