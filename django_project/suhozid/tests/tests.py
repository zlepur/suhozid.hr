"""
This file demonstrates writing tests using the unittest module. These will pass
when you run "manage.py test".

Replace this with more appropriate tests for your application.
"""

from datetime import datetime

from django.test import TestCase

from suhozid.utils import (
    format_coordinate,
    calculatePointBuffer,
    format_datetime
)
from django.contrib.gis import geos


class TestUtilsModule(TestCase):
    def test_fmt_coordinate_output(self):
        """
        Format coordinate output test
        """
        myCoord = geos.Point(15.6665164899465, 43.6488318926133)
        myOutput = format_coordinate(myCoord)

        self.assertEqual(myOutput, u'lat: 15.666516 lon: 43.648832')

        myCoord = geos.Point(15.12, -14)
        myOutput = format_coordinate(myCoord)

        self.assertEqual(myOutput, u'lat: 15.120000 lon: -14.000000')

    def test_fmt_coordiante_bad_input(self):
        """
        Format coordinate bad input test
        """
        myCoord = None
        myOutput = format_coordinate(myCoord)

        self.assertEqual(myOutput, None)

    def test_calculatePointBuffer_return(self):
        """
        Test outputcalculatePointBuffer return value
        """
        bbox = [1186689.719113, 5049919.669995, 2553995.280888, 6077233.330005]
        height = 840
        width = 1118
        clickX = 837
        clickY = 343

        expectedOutput = (
            u'POLYGON ((19.8008401205953781 45.1909439284290571, '
            '19.8008401205953781 45.2683170118597360, 19.9107034018300908 '
            '45.2683170118597360, 19.9107034018300908 45.1909439284290571, '
            '19.8008401205953781 45.1909439284290571))')

        self.assertEqual(
            expectedOutput,
            calculatePointBuffer(bbox, height, width, clickX, clickY).wkt
        )

        expectedOutput = (
            u'POLYGON ((19.7459084799780200 45.1522178889394894, '
            '19.7459084799780200 45.3069640563709015, 19.9656350424474489 '
            '45.3069640563709015, 19.9656350424474489 45.1522178889394894, '
            '19.7459084799780200 45.1522178889394894))')

        self.assertEqual(
            expectedOutput,
            calculatePointBuffer(
                bbox, height, width, clickX, clickY, pixel_offset=10).wkt
        )

    def test_format_datetime_output(self):
        """
        Test conversion of datetime object to EU time.
        """
        day = 25
        month = 3
        year = 2006
        hour = 23
        minute = 11
        second = 3
        expectedOutput = "23:11:03 25-03-2006"

        datetime_instance = datetime(
            year=year,
            month=month,
            day=day,
            hour=hour,
            minute=minute,
            second=second
        )
        output = format_datetime(datetime_instance)
        self.assertEqual(expectedOutput, output)

    def test_format_datetime_bad_output(self):
        """
        Test conversion of datetime object to EU time by passing bad input.
        """
        badInput = "garbage"
        expectedOutputOnBadInput = None

        badOutput = format_datetime(badInput)
        self.assertEqual(expectedOutputOnBadInput, badOutput)
